package Room;

import Person.Person;

import java.util.Map;
import java.util.HashMap;

public class LivingSpace extends Room {
    public String roomName;
    public String roomType;
    public int limit = 4;
    public Map<String, Person> occupants = new HashMap<String, Person>();

    public LivingSpace(String type, String name){
        super(type, name);
        this.roomName = name;
        this.roomType = type;
    }

    public String allocateRoom(Person person){
        if (occupants.size() <= this.limit){
            occupants.put(person.id, person);
            return String.format("Success!, %s added successfully.", person.personName());
        }
        else {
            return String.format("Error!, %s's room-limit reached.", roomName);
        }
    }

    public String deAllocateRoom(Person person){
        if (occupants.containsKey(person.id)){
            occupants.remove(person.id);
            return String.format("Success!, %s de-allocated from %s.", person.personName(), roomName);
        }
        else {
            return String.format("Error!, %s not in the room %s.", person.personName(), roomName);
        }
    }
}