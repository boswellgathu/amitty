package Person;

public class Staff extends Person {
    public String firstName, lastName, email, type, id;
    protected int cohort;
    public boolean wantsAccommodation;

    public Staff(String first, String last, String email, String type, int cohort ){
        super(first, last, email, type, cohort);
        this.wantsAccommodation = false;
    }
}
