package Person;

import java.util.UUID;

public class Person {
    public String firstName, lastName, email, type, id;
    public int cohort;
    public boolean wantsAccommodation;

    public Person(String first, String last, String email, String type, int cohort ){
        this.firstName = first;
        this.lastName = last;
        this.email = email;
        this.type = type;
        this.id = this.allocateId();
        this.cohort = cohort;
        this.wantsAccommodation = true;
    }

    private String allocateId() {
        return  UUID.randomUUID().toString().replaceAll("-", "");
    }
    public String personName() {
        return String.format("%s %s", firstName, lastName);
    }
}

