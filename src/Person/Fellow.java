package Person;

public class Fellow extends Person {
    public String firstName, lastName, email, type, id;
    protected int cohort;
    public boolean wantsAccommodation;

    public Fellow(String first, String last, String email, String type, int cohort , boolean accommodation ){
        super(first, last, email, type, cohort);
        this.wantsAccommodation = accommodation;
    }
}
