package com.gathu;

import Person.*;
import Room.*;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Main {
    public static Map<String, Person> allPeople = new HashMap<String, Person>();
    public static Map<String, Room> allRooms = new HashMap<String, Room>();


    public static String addPerson(String first, String last, String email, String type, int cohort, boolean accommodation) {
        Person newPerson;
        if (allPeople.containsKey(email)){
            return String.format("Error! A person with email: %s already exists", email);
        }
        else if (type.toLowerCase().equals("fellow")) {
            newPerson = new Fellow(first, last, email, type, cohort, accommodation);
            System.out.println(newPerson.wantsAccommodation);
            allPeople.put(newPerson.email, newPerson);
            // allocate
            allocatePerson(newPerson);
            return String.format("Fellow %s registered successfully", newPerson.personName());
        }
        else if (type.toLowerCase().equals("staff")){
            newPerson = new Staff(first, last, email, type, cohort);
            allPeople.put(newPerson.email, newPerson);
            System.out.println(newPerson.wantsAccommodation);
            // allocate
            allocatePerson(newPerson);
            return String.format("Staff %s registered successfully", newPerson.personName());
        }
        else {
            return String.format("type %s Not allowed!, accepted types are staff or fellow", type);
        }
    }

    public static String createRoom(String roomType, String roomName) {
        Room newRoom;
        if (!roomType.equals("office") && !roomType.equals("livingspace")){
            return String.format("Room of type %s cannot be created, accepted types are office and or livingspace", roomType);
        }
        if (allRooms.containsKey(roomName)){
            return String.format("Error! A room with the name: %s already exists!", roomName);
        }
        else {
            if (roomType.equals("office")){
                newRoom = new Office(roomType, roomName);
                allRooms.put(roomName, newRoom);
                return String.format("%s %s created successfully!", roomType, roomName);
            }
            else {
                newRoom = new LivingSpace(roomType, roomName);
                allRooms.put(roomName, newRoom);
                return String.format("%s %s created successfully!", roomType, roomName);
            }
        }
    }

    public static List<String> getRoomsByType(String roomType){
        List<String> gotRooms = new ArrayList<>();
        for (Map.Entry<String, Room> room : allRooms.entrySet()){
            if (room.getValue().roomType.equals(roomType)){
                gotRooms.add(room.getKey());
            }
        }
        return gotRooms;
    }

    public static String getRandomRoom(List<String> rooms) {
        Random random = new Random();
        int index = random.nextInt(rooms.size());
        return rooms.get(index);
    }

    public static String allocatePerson(Person person){
        List<String> allOffices = getRoomsByType("office");
        List<String> allLivingSpaces = getRoomsByType("livingspace");
        if (person.type.equals("fellow")){
//            String office = getRandomRoom(allOffices);
//            System.out.println(allRooms.get(office).allocateRoom(person));
//            System.out.println(allRooms.get(office).occupants);
            System.out.println(person.wantsAccommodation);

            if (person.wantsAccommodation) {
                String office = getRandomRoom(allOffices);
                System.out.println(allRooms.get(office));
            }
        }
        return "all the fellows got!";
    }

    public static void main(String[] args) {
        // creating office
        System.out.println(createRoom("office", "Valhalla"));
        // creating living-space
        System.out.println(createRoom("livingspace", "Roysambu"));
        // checking error handling for duplicate room names
        System.out.println(createRoom("livingspace", "Roysambu"));
        // checking error handling for unaccepted room type
        System.out.println(createRoom("malumbo", "Roysambu"));
        // adding a fellow
        System.out.println(addPerson("kamau", "maina", "maina.kayai@gmail.com","fellow", 234, true));
        //adding a staff
        System.out.println(addPerson("kamau", "maina", "maina.kayai@gmail.com","staff", 234, false));
        // objects
        System.out.println(allPeople);
        System.out.println(allRooms);

        getRoomsByType("office");
    }

}


